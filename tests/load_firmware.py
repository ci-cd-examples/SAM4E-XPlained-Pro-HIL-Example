import serial
from serial.tools import list_ports
import re
import time
import sys
import os
import fnmatch

# Raspberry Pi Libraries
import RPi.GPIO as GPIO

# Raspberry Pi pin number that controls the USB VBUS line
power_control_pin = 8

# Configuration for downstream device
Baud = '115200'
uut_vid = 0x03EB
uut_pid = 0x2404

# pin_num should be the phsyical pin number
# (i.e. versus BCM/Broadcom pin number)
def initPowerSupply(pin_num):
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(pin_num, GPIO.OUT)
    GPIO.output(pin_num, GPIO.HIGH)
    time.sleep(1)

def shutDownPowerSupply(pin_num):
    GPIO.output(pin_num, GPIO.LOW)

def cyclePower(pin_num):
    GPIO.output(pin_num, GPIO.LOW)
    time.sleep(1)
    GPIO.output(pin_num, GPIO.HIGH)
    time.sleep(1)

def clearATSAMFlash():
    COMPort = getPortByVIDPID(uut_vid, uut_pid)
    with serial.Serial(COMPort, Baud, timeout=1) as ser:
        ser.write("gpnvmclr\r".encode())
    time.sleep(1)

# Assumes you have only one device plugged in with the 
# VID and PID specified
# TO DO: Add error handling if device cannot be found
def getPortByVIDPID(VID, PID):
    for port in list_ports.comports():
        if (port.vid == VID and port.pid == PID):
            return port.device

# If the result code is anything but success (0), exit with specified code
def ExitIfFailure(resultCode, inst):
    if(resultCode > 0):
        sys.stderr.write(f"Exiting with exit code: {resultCode}")
        shutDownPowerSupply(inst)
        sys.exit(resultCode)

# Find a file given the pattern and start path
def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result

def load_firmware():
    # Enable USB VBUS line to target device
    print("Enabling VBUS to target USB device.")
    initPowerSupply(power_control_pin)
    # Perform the "GPNVMClr" command to erase device and cycle power
    print("Clearing device programming flash.")
    try:
        clearATSAMFlash()
    except:
        pass
    cyclePower(power_control_pin)
    
    # Find the COM Port associated with "Bossa Program Port"
    try:
        samba_vid = 0x03eb
        samba_pid = 0x6124
        pathToBossa = "bossac"
        bossaOptions = "-u -e -w -v -b --port=" + getPortByVIDPID(samba_vid, samba_pid)
        pathToBin = find('*.bin', '../')[0]
    except:
        shutDownPowerSupply(power_control_pin)
        raise
    
    # Run Bossa with the associated TCL file
    print("Running BOSSA to program device")
    cmd = f"{pathToBossa} {bossaOptions} {pathToBin}"
    try:
        ExitIfFailure(os.system(cmd), power_control_pin)
    except:
        shutDownPowerSupply(power_control_pin)
        raise
    # Power cycle device
    print("Power cycling device")
    cyclePower(power_control_pin)
    time.sleep(1)
    
    # Turn off device
    shutDownPowerSupply(power_control_pin)
    
if __name__ == '__main__':
    load_firmware()
