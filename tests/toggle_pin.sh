#!/bin/sh
if [ $# -eq 0 ]; then
  echo "Usage: $0 <Pin Number>" >&2
  exit 1
fi

echo "Powering on target device."

# Toggle output pin via bash
# Source: https://raspberrypi-aa.github.io/session2/bash.html
# Exports pin to userspace
echo "$1" > /sys/class/gpio/unexport || true
echo "$1" > /sys/class/gpio/export

# Sets pin <Pin Number> as an output
echo "out" > /sys/class/gpio/gpio$1/direction

# Sets pin <Pin Number> to low
echo "0" > /sys/class/gpio/gpio$1/value

# Sets pin <Pin Number> to high
echo "1" > /sys/class/gpio/gpio$1/value