#!/bin/bash

# Make a test directory
mkdir /home/test && cd /home/test

# Pull the artifacts
wget https://gitlab.com/ci-cd-examples/SAM4E-XPlained-Pro-HIL-Example/-/jobs/480114001/artifacts/raw/SAM4E_XPlained_Pro_HIL_Example/Debug/SAM4E_XPlained_Pro_HIL_Example.bin

# Install necessary Python packages
wget https://gitlab.com/ci-cd-examples/SAM4E-XPlained-Pro-HIL-Example/-/raw/master/tests/requirements.txt
pip3 install -r requirements.txt

# Run the firmware loader script
wget https://gitlab.com/ci-cd-examples/SAM4E-XPlained-Pro-HIL-Example/-/raw/master/tests/load_firmware.py
python3 load_firmware.py