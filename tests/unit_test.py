import pytest
import xunitparser
import time
import platform
import sys
import re
import serial
from serial.tools import list_ports
from distutils import util

# Raspberry Pi Libraries
if (getattr(platform.uname(), "machine").startswith("arm")):
    import RPi.GPIO as GPIO

# Raspberry Pi pin number that controls the USB VBUS line
power_control_pin = 8

# Configuration for downstream device
Baud = '115200'
uut_vid = 0x03EB
uut_pid = 0x2404

# Initialize the device
def setup_module():
    # Turn on power supply. In this case it's just enabling a GPIO.
    print("Initializing power supply.")
    if (getattr(platform.uname(), "machine").startswith("arm")):
        initPowerSupply(power_control_pin)
    # Get the USB serial port number of the downstream device
    pytest.global_variable_1 = getPortByVIDPID(uut_vid, uut_pid)
    time.sleep(1)

# Clplatforme all sessions and devices after tests have completed
def teardown_module():
    if (getattr(platform.uname(), "machine").startswith("arm")):
        GPIO.output(power_control_pin, GPIO.LOW)

def test_setBit():
    print("Issuing setbit command.")
    print("Response: %s" % getSerialData(pytest.global_variable_1, Baud, "setbit\r"))
    assert (getSerialData(pytest.global_variable_1, Baud, "getbit\r") == 'Value of testBit is 1.')

def test_clearBit():
    print("Issuing bit command.")
    print("Response: %s" % getSerialData(pytest.global_variable_1, Baud, "clearbit\r"))
    assert (getSerialData(pytest.global_variable_1, Baud, "getbit\r") == 'Value of testBit is 0.')

# Assumes you have only one device plugged in with the 
# VID and PID specified
# TO DO: Add error handling if device cannot be found
def getPortByVIDPID(VID, PID):
    for port in list_ports.comports():
        if (port.vid == VID and port.pid == PID):
            return port.device

# Acquire telemetry by sending the requested command
# and return read data over USB COM port
def getSerialData(COMPort, Baud, command):
    with serial.Serial(COMPort, Baud, timeout=1) as ser:
        ser.write(command.encode())
        time.sleep(0.5)
        return(ser.readline().decode("utf-8").strip())

# pin_num should be the physical pin number
# (i.e. versus BCM/Broadcom pin number)
def initPowerSupply(pin_num):
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(pin_num, GPIO.OUT)
    GPIO.output(pin_num, GPIO.LOW)
    time.sleep(1)
    GPIO.output(pin_num, GPIO.HIGH)
    time.sleep(1)

if __name__ == "__main__":
    print("Usage: pytest %s --junitxml=testLog.xml" % __file__)